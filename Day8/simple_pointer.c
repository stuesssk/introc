#include <stdio.h>


int main(void)
{

	double radius = 5.789;
	double *rad_ptr = &radius;

	printf("&Radius: %p\n",(void *)&radius);
	printf("rad_ptr: %p\n",(void *)rad_ptr);

	printf("Radius  : %.3lf\n",radius);
	printf("*rad_ptr: %.3lf\n",*rad_ptr);
}
