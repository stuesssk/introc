#include <stdio.h>
#include "card.c"
#include "card.h"

struct card card_cheat(struct card c);

int main(void)
{
	struct card hole_card = {2,"Clubs"};

	char buf[32];
	
	card_format(buf, sizeof(buf), hole_card);
	printf("Before Cheat: %s\n",buf);

	hole_card = card_cheat(hole_card);
	card_format(buf, sizeof(buf), hole_card);
	printf("After Cheat: %s\n",buf);	
}	

struct card card_cheat(struct card c)
{
	c.rank = 14;
	return c;

}
