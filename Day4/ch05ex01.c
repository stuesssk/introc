#include <stdio.h>
#include <ctype.h>


int main(void)
{

	int c;
	c = getchar();
	while (c != -1){
		if (isalpha(c)){
			putchar(c);
		}
		c = getchar();
	}
} 
