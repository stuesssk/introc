#include <stdio.h>

int main(void)
{
    int able = 2, baker = 3, charlie = 5;

    printf("a = %d\n",able);
    printf("b = %d\n",baker);
    printf("c = %d\n",charlie);

    printf("a + b = %d\n",able + baker);

    charlie += 2; //charlie = charlie + 2
    
    printf("c / b = %d\n", charlie / baker);
    printf("c / a = %d\n", charlie / able);

    printf("c %% b = %d\n", charlie % baker);
    printf("c %% a = %d\n", charlie % able);


    // Using floats as storage would cause
    // yankke +  zulu to dro pthe yankee portion
    double xray = 3.1, yankee = 0.00000041, zulu = 590000;
    
    printf("x = %.10lf\n",xray);
    printf("y = %.10lf\n",yankee);
    printf("z = %.10lf\n",zulu);

    printf("%.10f\n", zulu + yankee);

}
