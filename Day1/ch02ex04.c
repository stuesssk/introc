#include <stdio.h>

int sum_of_squares(int a,int b)
{
    a *= a;
    b *= b;
    return (a + b);
}

int main(void)
{
    int a = 5, b = 6, c;
    c = sum_of_squares(a,b);
    printf("The sum of squares of %d and %d = %d\n",a,b,c);
}


