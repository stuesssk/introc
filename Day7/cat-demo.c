#include <stdio.h>
#include <sysexits.h>


int main(int argc, char *argv[])
{
    if (argc != 2 ){
        fprintf(stderr,"Usage: %s <values>\n", argv[0]);
        return 1;
    }
	


	// Open file
	//(Name of file, string indicating mode)
	FILE *fp = fopen(argv[1],"r");
	
	// fopen can return null or error
	// NULL we can treat like a false value
	if (!fp){
		perror("Unable to open file for reading");
		return EX_NOINPUT;
	}

	
	char buf[128];
	// print lines form the file
	while (fgets(buf, sizeof(buf), fp)){
		 printf("%s",buf);
	}
	// close the file
	if (fclose(fp)){
		perror("Unable to close file!?");
		return EX_IOERR;
	}

}
