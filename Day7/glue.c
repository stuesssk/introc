#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>


int main(int argc, char *argv[])
{
    if (argc != 3 ){
        fprintf(stderr,"Usage: %s <file1> <file2>\n", argv[0]);
        return 1;
    }

	// Open file
	//(Name of file, string indicating mode)
	FILE *fp1 = fopen(argv[1],"r");


	int ret_code = EX_OK;
	// fopen can return null or error
	// NULL we can treat like a false value
	if (!fp1){
		perror("Unable to open file 1 for reading");
		ret_code = EX_NOINPUT;
		goto leave;
	}
	FILE *fp2 = fopen(argv[2],"r");	
	if (!fp2){
		perror("Unable to open file 2 for reading");	
		ret_code =  EX_NOINPUT;
		goto fp1_cleanup;
	}


	char buf1[128];
	char buf2[128];
	// print lines form the file
	while (fgets(buf1, sizeof(buf1), fp1)){
		printf("%s",buf1);
	}
	while (fgets(buf2, sizeof(buf2), fp2)){
		printf("%s",buf2);
	}
cleanup:
	// close the file
	if (fclose(fp2)){
		perror("Unable to close file 2!?");
		ret_code = EX_IOERR;
	}
fp1_cleanup:
	if (fclose(fp1)){
		perror("Unable to close file 1!?");
		ret_code = EX_IOERR;
	}
leave:
	return ret_code;
}
