#include "cards.h"

#include <stdio.h>
#include <string.h>

// Structures can be passed as parameters
bool card_format(char output[], size_t sz, struct card c)
{
	if(sz < 18) {
		return false;
	}

	switch(c.rank) {
		case 14:
			snprintf(output, sz, "Ace");
			break;
		case 13:
			snprintf(output, sz, "King");
			break;
		case 12:
			snprintf(output, sz, "Queen");
			break;
		case 11:
			snprintf(output, sz, "Jack");
			break;
		default:
			snprintf(output, sz, "%d", c.rank);
	}

	strncat(output, " of ", sz - strlen(output) - 1);
	strncat(output, c.suit, sz - strlen(output) - 1);

	return true;
}

int card_comp(struct card a, struct card b)
{
	int test;
	if (a.rank>b.rank){
		test = 1;
		return test;
	}else if (a.rank==b.rank){
		test = 0;
		return test;
	}else{
		test = -1;
		return test;
	}


}
