
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cards.h"
#include "deck.h"

int main(void)
{
	struct deck pile = deck_create();
	char buf[18];

	for(int n = 0; n < 52; ++n) {
		card_format(buf, sizeof(buf), pile.cards[n]);

		puts(buf);
	}


	printf("Input number of cards do deal");

	
	size_t amount = strtol(fgets(buf,sizeof(buf),stdin),NULL,10);
	struct card hand;
	//for (int i = 0; i < amount; ++i){		
	deck_deal(pile,hand,amount);
	
	
	struct card a;
	struct card b; 
	a.rank = pile.cards[12].rank;
	strncpy(a.suit,pile.cards[12].suit,sizeof(pile.cards[12].suit));
	b.rank = pile.cards[36].rank;
	strncpy(b.suit,pile.cards[36].suit,sizeof(pile.cards[36].suit));

 	int test = card_comp(a,b);
	switch (test){
		case 1:
			printf("Card a greater than card b.\n");
			break;
		case 0:
			printf("Card a equals card b.\n");
			break;
		case -1:
			printf("Card a less than card b.\n");
	}
}
