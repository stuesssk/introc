#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct money {
	int dollar;
	int cent;
};

struct money init(int dollars, int cents)
{
	struct money value;
	value.dollar = dollars;
	value.cent = cents;
	return value;

}

struct money input(void)
{
	
	char buff[20];
	int dollar, cent;
	
	dollar= strtol(fgets(buff,sizeof(buff),stdin),NULL,10);
	cent = strtol(fgets(buff,sizeof(buff),stdin),NULL,10);
	struct money c = init(dollar, cent);
	return c;
	

}
struct money add( struct money a, struct money b)
{
	struct money c;
	c.cent = (a.cent+b.cent)%100;
	int cent_dollar = (a.cent+b.cent)/100;
	c.dollar = a.dollar +b.dollar+cent_dollar;
	return c;


}

int main(void)
{
	struct money a = init(5,10);
	printf("$%d.%d\n", a.dollar, a.cent);
	//char buff[16];
	
	struct money b = input();
	printf("$%d.%d\n", b.dollar, b.cent);
	struct money c = add(a,b);
	printf("$%d.%d\n", c.dollar, c.cent);	


}
