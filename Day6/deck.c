#include "deck.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

struct deck deck_create(void)
{
	struct deck d;
	for(int s = 0; s < 4; ++s) {
		for(int rank = 2; rank < 15; ++rank) {
			size_t i = s*13 + (rank - 2);

			switch(s) {
				case 0:
					strncpy(d.cards[i].suit, "Clubs", sizeof(d.cards[i].suit));
					break;
				case 1:
					strncpy(d.cards[i].suit, "Diamonds", sizeof(d.cards[i].suit));
					break;
				case 2:
					strncpy(d.cards[i].suit, "Hearts", sizeof(d.cards[i].suit));
					break;
				case 3:
					strncpy(d.cards[i].suit, "Spades", sizeof(d.cards[i].suit));
					break;
			}

			d.cards[i].rank = rank;
		}
	}

	return d;
}

void deck_deal(struct deck d, struct card hand, size_t amount)
{
	// Could print a random card number for each iteration in for loop under
	// the amount passed
	for (size_t i = 0; 	i< amount; ++i){
		//hand.rank[i] = d.cards[i].rank;	
		//hand.suit[i] = d.cards[i].suit;
		printf("%d of %s\n",d.cards[i].rank,d.cards[i].suit);
	}

}		





