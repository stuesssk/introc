#include <stdio.h>

const int WARMING = 75;

int iterator(void);

//int main(int argc, int argv[])
int main(void)
{

    int value = iterator();
    printf("%d\n",value);

    value = iterator();
    printf("%d\n",value);

    value = iterator();
    printf("Warming: %d\n",WARMING);
}


int iterator(void)
{
    printf("Warming in iter: %d\n",WARMING);
    static int x = 0;
    x += 1;

    return x;

}
