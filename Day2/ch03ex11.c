#include <stdio.h>

int main(void)
{
    int i = 10, factorial = 1;
    while (i >= 1){
        factorial = factorial * i--;
    }
    printf ("10! = %d\n",factorial);
}
