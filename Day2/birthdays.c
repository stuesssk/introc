#include <stdio.h>

int main(void)
{
    int year = 1969;

    // Note that we "cuddle" the closing brace of the first part of the 
    // conditional with any additional parts.
    if(year >2000) {
        printf("Just out of diapers\n");
    } else if (year > 1990) {
        printf("Radicale Dude.\n");
    } else {
        printf("Wise Beyond your years.\n");
    }
}

