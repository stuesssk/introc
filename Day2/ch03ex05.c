#include <stdio.h>

int main(void)
{
    int sum = 0, i = 1;

    while (i <= 100){
        sum += i++;
    }
    printf("The sum total is %d\n", sum);
}
