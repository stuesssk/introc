#include <stdio.h>

int main(void)
{
    int i;
    printf("Number\tSquare\tCube\n");
    for ( i = 20; i <= 60; i += 2)
    {
        int square = i * i;
        int cube = i * i * i;
        printf("%d\t%d\t%d\n",i,square,cube);
    }
}
