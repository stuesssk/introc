#include <stdio.h>
#include <stdlib.h>


int main(int argc, char *argv[])
{
    if (argc != 3 ){
        //TODO: should print to error
        printf("Usage: %s <values>\n", argv[0]);
        //TODO: Should return useful error code 
        return 1;
    }
    double weight = strtol(argv[1], NULL, 10)/ 2.20462;
    double height = strtol(argv[2], NULL, 10)* 0.0254;
    double bmi = weight / (height*height);

    //printf("%lf\n%lf\n%lf\n",weight,height,bmi);
    if (bmi <18.5){
        printf("BMI is %.2lf\nUnderwieght\n",bmi);
    }else if (bmi >= 18.5 && bmi < 25){
        printf("BMI is %.2lf\nNormal\n",bmi);
    }else if (bmi >= 25 && bmi < 30){
        printf("BMI is %.2lf\nOverweight\n",bmi);
    }else{
        printf("BMI is %.2lf\nObese\n",bmi);
    }
}
