#include <stdio.h>
#include <stdlib.h>

size_t array_length(int array[]);

int main(void)
{
    int grades[] = { 75, 100, 80 , 36 };
    printf("Number of elements is %zd\n", array_length(grades));
}

size_t array_length(int array[])
{
    // Only the memory address for the array is passed, the size is just the 
    // size of memory used to supply the memory address.
    return sizeof(array) / sizeof(array[0]);
}
