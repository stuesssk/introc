#include <stdio.h>

int main(void)
{
    int sum = 0, i;

    for ( i = 1; i <=100; i++)
    {
        if (i % 5 == 0){

            sum += i;
        }
    }
    printf("The sum total is %d\n", sum);
}
