#include <stdio.h>

int main(void)
{
    int a = 5, b = 10;
    double x = 25.5, y = 20;
    printf("%d + %d = %d\n",a,b,a+b );
    printf("%d * %d = %d\n",a,b,a*b );
    printf("%.2f + %.2f = %.2f\n",x,y,x+y );
    printf("%.2f * %.2f = %.2f\n",x,y,x+y );
}
